package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.AbstractBusinessEntity;

import java.util.List;

public interface IBusinessEntityService<T extends AbstractBusinessEntity> extends IService<T> {

    long size(@Nullable Long userId);

    boolean isEmpty(@Nullable Long userId);

    @NotNull
    List<T> findAll(@Nullable Long userId);

    @NotNull
    T findByName(@Nullable Long userId, @Nullable String name);

    @NotNull
    T editById(@Nullable Long id, @Nullable String name, @Nullable String description);

    @NotNull
    T editByName(@Nullable Long userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable Long userId);

    void removeByName(@Nullable Long userId, @Nullable String name);

    @NotNull
    T startById(@Nullable Long id);

    @NotNull
    T endById(@Nullable Long id);

    @NotNull
    List<T> sortByName(@Nullable Long userId);

    @NotNull
    List<T> sortByStartDate(@Nullable Long userId);

    @NotNull
    List<T> sortByEndDate(@Nullable Long userId);

    @NotNull
    List<T> sortByStatus(@Nullable Long userId);

}
