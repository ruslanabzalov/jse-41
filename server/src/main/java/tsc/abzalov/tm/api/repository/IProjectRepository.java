package tsc.abzalov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.Project;

import java.sql.Timestamp;
import java.util.List;

public interface IProjectRepository {

    @Select("SELECT COUNT(id) " +
            "FROM project;")
    long projectsSize();

    @Select("SELECT COUNT(id) " +
            "FROM project " +
            "WHERE user_id = #{userId};")
    long projectsSizeByUserId(@NotNull @Param("userId") Long userId);

    @Insert("INSERT INTO project " +
            "(id, name, description, start_date, end_date, status, user_id) " +
            "VALUES " +
            "(#{id}, #{name}, #{description}, #{startDate}, #{endDate}, #{status}, #{userId});")
    void createProject(@NotNull @Param("id") Long id,
                       @NotNull @Param("name") String name,
                       @NotNull @Param("description") String description,
                       @Nullable @Param("startDate") Timestamp startDate,
                       @Nullable @Param("endDate") Timestamp endDate,
                       @NotNull @Param("status") String status,
                       @NotNull @Param("userId") Long userId);

    @Delete("DELETE FROM project;")
    void clearAllProjects();

    @Delete("DELETE FROM project " +
            "WHERE user_id = #{userId};")
    void clearProjectByUserId(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project;")
    List<Project> findAllProjects();

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project " +
            "WHERE user_id = #{userId};")
    List<Project> findProjectsByUserId(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project " +
            "WHERE id = #{id} " +
            "LIMIT 1;")
    Project findProjectById(@NotNull @Param("id") Long id);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project " +
            "WHERE user_id = #{userId} AND name = #{name} " +
            "LIMIT 1;")
    Project findProjectByName(@NotNull @Param("userId") Long userId,
                              @NotNull @Param("name") String name);

    @Delete("DELETE FROM project " +
            "WHERE id = #{id};")
    void removeProjectById(@NotNull @Param("id") Long id);

    @Delete("DELETE FROM project " +
            "WHERE user_id = #{userId} AND name = #{name};")
    void removeProjectByName(@NotNull @Param("userId") Long userId,
                             @NotNull @Param("name") String name);

    @Update("UPDATE project " +
            "SET name = #{name}, description = #{description} " +
            "WHERE id = #{id};")
    void editProjectById(@NotNull @Param("id") Long id,
                         @NotNull @Param("name") String name,
                         @NotNull @Param("description") String description);

    @Update("UPDATE project " +
            "SET description = #{description} " +
            "WHERE user_id = #{userId} AND name = #{name};")
    void editProjectByName(@NotNull @Param("userId") Long userId,
                           @NotNull @Param("name") String name,
                           @NotNull @Param("description") String description);

    @Update("UPDATE project " +
            "SET start_date = #{startDate}, status = #{status} " +
            "WHERE id = #{id};")
    void startProjectById(@NotNull @Param("id") Long id,
                          @NotNull @Param("startDate") Timestamp startDate,
                          @NotNull @Param("status") String status);

    @Update("UPDATE project " +
            "SET end_date = #{endDate}, status = #{status} " +
            "WHERE id = #{id};")
    void endProjectById(@NotNull @Param("id") Long id,
                        @NotNull @Param("endDate") Timestamp endDate,
                        @NotNull @Param("status") String status);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project " +
            "WHERE user_id = #{userId} " +
            "ORDER BY name;")
    List<Project> sortProjectsByName(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project " +
            "WHERE user_id = #{userId} " +
            "ORDER BY start_date;")
    List<Project> sortProjectsByStartDate(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project " +
            "WHERE user_id = #{userId} " +
            "ORDER BY end_date;")
    List<Project> sortProjectsByEndDate(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id " +
            "FROM project " +
            "WHERE user_id = #{userId} " +
            "ORDER BY status;")
    List<Project> sortProjectsByStatus(@NotNull @Param("userId") Long userId);

}
