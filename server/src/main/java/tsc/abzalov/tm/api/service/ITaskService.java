package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.dto.Task;

public interface ITaskService extends IBusinessEntityService<Task> {
}
