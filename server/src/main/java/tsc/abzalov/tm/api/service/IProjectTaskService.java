package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.Project;
import tsc.abzalov.tm.dto.Task;

import java.util.List;

public interface IProjectTaskService {

    boolean hasData(@Nullable Long userId);

    void addTaskToProjectById(@Nullable Long taskId, @Nullable Long projectId);

    @NotNull
    Project findProjectById(@Nullable Long id);

    @NotNull
    Task findTaskById(@Nullable Long id);

    @NotNull
    List<Task> findProjectTasksById(@Nullable Long userId, @Nullable Long projectId);

    void deleteProjectById(@Nullable Long id);

    void deleteProjectTasksById(@Nullable Long userId, @Nullable Long projectId);

}
