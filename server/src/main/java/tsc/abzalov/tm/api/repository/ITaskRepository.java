package tsc.abzalov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.dto.Task;

import java.sql.Timestamp;
import java.util.List;

public interface ITaskRepository {

    @Select("SELECT COUNT(id) " +
            "FROM task;")
    long tasksSize();

    @Select("SELECT COUNT(id) " +
            "FROM task " +
            "WHERE user_id = #{userId};")
    long tasksSizeByUserId(@NotNull @Param("userId") Long userId);

    @Insert("INSERT INTO task " +
            "(id, name, description, start_date, end_date, status, user_id, project_id) " +
            "VALUES " +
            "(#{id}, #{name}, #{description}, #{startDate}, #{endDate}, #{status}, #{userId}, #{projectId});")
    void createTask(@NotNull @Param("id") Long id,
                    @NotNull @Param("name") String name,
                    @NotNull @Param("description") String description,
                    @Nullable @Param("startDate") Timestamp startDate,
                    @Nullable @Param("endDate") Timestamp endDate,
                    @NotNull @Param("status") String status,
                    @NotNull @Param("userId") Long userId,
                    @Nullable @Param("projectId") Long projectId);

    @Delete("DELETE FROM task;")
    void clearAllTasks();

    @Delete("DELETE FROM task " +
            "WHERE user_id = #{userId};")
    void clearTasksByUserId(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task;")
    List<Task> findAllTasks();

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE user_id = #{userId};")
    List<Task> findTasksByUserId(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE id = #{id} " +
            "LIMIT 1;")
    Task findTaskById(@NotNull @Param("id") Long id);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE user_id = #{userId} AND name = #{name} " +
            "LIMIT 1;")
    Task findTaskByName(@NotNull @Param("userId") Long userId,
                        @NotNull @Param("name") String name);

    @Delete("DELETE FROM task " +
            "WHERE id = #{id};")
    void removeTaskById(@NotNull @Param("id") Long id);

    @Delete("DELETE FROM task " +
            "WHERE user_id = #{userId} AND name = #{name};")
    void removeTaskByName(@NotNull @Param("userId") Long userId,
                          @NotNull @Param("name") String name);

    @Update("UPDATE task " +
            "SET name = #{name}, description = #{description} " +
            "WHERE id = #{id};")
    void editTaskById(@NotNull @Param("id") Long id,
                      @NotNull @Param("name") String name,
                      @NotNull @Param("description") String description);

    @Update("UPDATE task " +
            "SET description = #{description} " +
            "WHERE user_id = #{userId} AND name = #{name};")
    void editTaskByName(@NotNull @Param("userId") Long userId,
                        @NotNull @Param("name") String name,
                        @NotNull @Param("description") String description);

    @Update("UPDATE task " +
            "SET start_date = #{startDate}, status = #{status} " +
            "WHERE id = #{id};")
    void startTaskById(@NotNull @Param("id") Long id,
                       @NotNull @Param("startDate") Timestamp startDate,
                       @NotNull @Param("status") String status);

    @Update("UPDATE task " +
            "SET end_date = #{endDate}, status = #{status} " +
            "WHERE id = #{id};")
    void endTaskById(@NotNull @Param("id") Long id,
                     @NotNull @Param("endDate") Timestamp endDate,
                     @NotNull @Param("status") String status);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE user_id = #{userId} " +
            "ORDER BY name;")
    List<Task> sortTasksByName(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE user_id = #{userId} " +
            "ORDER BY start_date;")
    List<Task> sortTasksByStartDate(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE user_id = #{userId} " +
            "ORDER BY end_date;")
    List<Task> sortTasksByEndDate(@NotNull @Param("userId") Long userId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE user_id = #{userId} " +
            "ORDER BY status;")
    List<Task> sortTasksByStatus(@NotNull @Param("userId") Long userId);

    @Update("UPDATE task " +
            "SET project_id = #{project_id} " +
            "WHERE id = {id};")
    void addTaskToProjectById(@NotNull @Param("id") Long id,
                              @NotNull @Param("projectId") Long projectId);

    @Nullable
    @Select("SELECT id, name, description, start_date, end_date, status, user_id, project_id " +
            "FROM task " +
            "WHERE user_id = #{userId} AND project_id = #{projectId};")
    List<Task> findProjectTasksById(@NotNull @Param("userId") Long userId,
                                    @NotNull @Param("projectId") Long projectId);

    @Update("UPDATE task " +
            "SET project_id = NULL " +
            "WHERE user_id = #{userId} AND project_id = #{projectId};")
    void deleteProjectTasksById(@NotNull @Param("userId") Long userId,
                                @NotNull @Param("projectId") Long projectId);

}
