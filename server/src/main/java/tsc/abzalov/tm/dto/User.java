package tsc.abzalov.tm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.LiteralConst.*;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class User extends AbstractEntity implements Serializable, Cloneable {

    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = USER;

    @Column
    @Nullable
    private String firstname;

    @Column
    @Nullable
    private String lastname;

    @Column
    @Nullable
    private String email;

    @Column(name = "locked_flag")
    private boolean lockedFlag = false;

    public boolean isLocked() {
        return lockedFlag;
    }

    @Nullable
    @Override
    public User clone() {
        try {
            return (User) super.clone();
        } catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull val correctLogin = Optional.ofNullable(login).orElse(DEFAULT_LOGIN);
        @NotNull val correctRoleName = role.getRoleName();
        @NotNull val correctFirstName = Optional.ofNullable(firstname).orElse(DEFAULT_FIRSTNAME);
        @NotNull val correctLastName = Optional.ofNullable(lastname).orElse(DEFAULT_LASTNAME);
        @NotNull val correctEmail = Optional.ofNullable(email).orElse(DEFAULT_EMAIL);
        @NotNull val correctUserStatus = (isLocked()) ? LOCKED : ACTIVE;

        return "[ID: " + getId() +
                "; Login: " + correctLogin +
                "; Role: " + correctRoleName +
                "; First Name: " + correctFirstName +
                "; Last Name: " + correctLastName +
                "; Email: " + correctEmail +
                "; Status: " + correctUserStatus + "]";
    }

}
