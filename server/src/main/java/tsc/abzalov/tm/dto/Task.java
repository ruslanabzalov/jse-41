package tsc.abzalov.tm.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Task extends AbstractBusinessEntity implements Serializable, Cloneable {

    @Nullable
    @Column(name = "project_id")
    private Long projectId;

    @Nullable
    @Override
    public Task clone() {
        try {
            return (Task) super.clone();
        }
        catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull val superStringInterpretation = super.toString();
        return superStringInterpretation.replace("]", "; Project ID: " + this.projectId + "]");
    }

}
