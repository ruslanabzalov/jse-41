package tsc.abzalov.tm.exception.system;

import tsc.abzalov.tm.exception.AbstractException;

public final class PropertiesAreNotExistException extends AbstractException {

    public PropertiesAreNotExistException() {
        super("Properties file \"application.properties\" is not exist!");
    }

}
