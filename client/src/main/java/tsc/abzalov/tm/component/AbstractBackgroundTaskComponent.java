package tsc.abzalov.tm.component;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.service.IServiceLocator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public abstract class AbstractBackgroundTaskComponent {

    protected static final int INITIAL_DELAY = 1;

    protected static final int BACKUP_DELAY = 30;

    protected static final int SCANNER_DELAY = 5;

    @NotNull
    protected static final String COMMANDS_FILES_PATH = "commands/";

    @NotNull
    @Getter(value = AccessLevel.PROTECTED)
    private final IServiceLocator serviceLocator;

    @NotNull
    @Getter(value = AccessLevel.PROTECTED)
    private final ScheduledExecutorService scheduledExecutorService;

    public AbstractBackgroundTaskComponent(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public abstract void run();

    protected void executeCommand(@NotNull final String commandName) {
        @NotNull val commandService = getServiceLocator().getCommandService();
        @NotNull val command = commandService.getCommandByName(commandName);
        command.execute();
    }

}
